# Setup
Download first project 

# Install npm command
npm install or npm i

# To build and run
ng serve or ng serve --open

# AuthorListAssessement
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.0.4.

## Installation
1. clone the repository: `git clone https://gitlab.com/amit.badade/author-list-assessment.git`
2. Install dependencies: `npm install`
3. Run the project: `ng serve`

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Technologies Used
1) Angular
1) HTML5
1) SCSS

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Credits
This project was created by Amit Badade as part of a assessment in Angular development.
 
Feel free to customize this template with specific details about your project. If you have any further questions or need assistance, please don't hesitate to ask.
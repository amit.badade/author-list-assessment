import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthorRoutingModule } from './author-routing.module';
import { AuthorComponent } from './author.component';
import { ScrollDirective } from './author-directive';

@NgModule({
  imports: [
    CommonModule,
    AuthorRoutingModule,
    ScrollDirective,
  ],
  declarations: [AuthorComponent]
})
export class AuthorModule { }


import {
  Component,
  EventEmitter,
  OnInit,
  Output,
} from "@angular/core";
import { ScrollService } from "../Services/author.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-author",
  templateUrl: "./author.component.html",
  styleUrls: ["./author.component.scss"],
})
export class AuthorComponent implements OnInit {
  myTableHeader: any;
  count: number = 0;
  @Output() scrollingFinished = new EventEmitter<void>();

  constructor(private scrollService: ScrollService, private router: Router) {}

  ngOnInit() {
    this.getDataServer();
  }

  getDataServer() {
    this.scrollService.getAllData().subscribe((data: any) => {
      this.myTableHeader = data;
    });
  }

  onScrollingFinished() {
    this.scrollingFinished.emit();
    this.scrollService.moreData(this.count).subscribe((data: any) => {
      data.forEach((element: any) => {
        this.myTableHeader.push(element);
      });
      this.count = this.count + 1;
    });
  }

  navigateToDetail(row: any) {
    this.router.navigate(["authorDetails"], {
      queryParams: { id: row.id },
    });
  }
}

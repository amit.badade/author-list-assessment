import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable()
export class ScrollService {
  constructor(private http: HttpClient) {}

  getAllData(): Observable<any> {
    return this.http.get("https://picsum.photos/v2/list");
  }

  moreData(pageNumber: number): Observable<any> {
    return this.http.get("https://picsum.photos/v2/list?page="+ pageNumber +"?limit=100");
  }

  loadDetail(id: number): Observable<any> {
    return this.http.get("https://picsum.photos/id/" + id + "/info");
  }
}

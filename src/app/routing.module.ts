import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  {
    path: "author",
    loadChildren: () =>
      import("./author/author.module").then(
        (m) => m.AuthorModule
      ),
  },
  {
    path: "authorDetails",
    loadChildren: () =>
      import(
        "./author-details/author-details/author-details.module"
      ).then((m) => m.AuthorDetailsModule),
  },
  {
    path: "",
    redirectTo: "author",
    pathMatch: "full",
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: false })],
  exports: [RouterModule],
  providers: [],
})
export class AppRoutingModule {}

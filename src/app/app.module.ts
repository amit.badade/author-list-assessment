import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ScrollService } from "./Services/author.service";
import { HttpClientModule } from "@angular/common/http";
import { ScrollDirective } from "./author/author-directive";
import { AuthorModule } from "./author/author.module";
import { AuthorDetailsModule } from "./author-details/author-details/author-details.module";
import { NavigationService } from "./Services/navigation.service";

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    ScrollDirective,
    AppRoutingModule,
    AuthorModule,
    AuthorDetailsModule,
  ],
  providers: [ScrollService,NavigationService],
  bootstrap: [AppComponent],
})
export class AppModule {}

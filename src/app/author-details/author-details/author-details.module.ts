import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthorDetailsRoutingModule } from './author-routing.module';
import { AuthorDetailsComponent } from './author-details.component';

@NgModule({
  imports: [
    CommonModule,
    AuthorDetailsRoutingModule
  ],
  declarations: [AuthorDetailsComponent]
})
export class AuthorDetailsModule { }


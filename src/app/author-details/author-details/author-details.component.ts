import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ScrollService } from "src/app/Services/author.service";
import { NavigationService } from "src/app/Services/navigation.service";

@Component({
  selector: "app-author-details",
  templateUrl: "./author-details.component.html",
  styleUrls: ["./author-details.component.scss"],
})
export class AuthorDetailsComponent implements OnInit {
  myDetails: any;

  constructor(
    private route: ActivatedRoute,
    private scrollService: ScrollService,
    private navigation: NavigationService,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe((params: any) => {
      this.getdetails(params["id"]);
    });
  }

  getdetails(id: number) {
    this.scrollService.loadDetail(id).subscribe((data: any) => {
      this.myDetails = data;
    });
  }

  backToList() {
    //this.navigation.back();
     this.router.navigate(["author"]);
  }
}
